<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchFisicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sch_fisico', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_scheda')->nullable()->index('id_scheda_fisico_idx');
			$table->decimal('altezza', 3)->nullable();
			$table->decimal('peso', 3)->nullable();
			$table->decimal('massa_grassa', 3)->nullable();
			$table->decimal('massa_magra', 3)->nullable();
			$table->dateTime('data_insert')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sch_fisico');
	}

}
