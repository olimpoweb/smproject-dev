<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Accessi extends Model
{
    protected $table = 'ana_accessi';

    public function loadlist()
    {
        $data = DB::table('ana_accessi')
        ->join('ana_schede', 'ana_schede.id', '=', 'scheda_id')
        ->join('ana_livelli', 'ana_livelli.id', '=', 'livello_id')
        ->select('ana_accessi.*', 'cognome','nome','descrizione as livello')
        ->orderBy('cognome', 'asc')
        ->get();

        return $data;
    }
}
