<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnaSchedeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ana_schede', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('cognome')->nullable();
			$table->string('nome')->nullable();
			$table->date('data_nascita')->nullable();
			$table->dateTime('create_date')->nullable();
			$table->dateTime('update_date')->nullable();
			$table->integer('livello_id')->nullable()->index('id_livello_scheda_idx');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ana_schede');
	}

}
