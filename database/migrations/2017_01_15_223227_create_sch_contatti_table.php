<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchContattiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sch_contatti', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('indirzzo')->nullable();
			$table->string('citta')->nullable();
			$table->integer('cap')->nullable();
			$table->string('nazione')->nullable();
			$table->string('telefono', 50)->nullable();
			$table->string('cellulare', 50)->nullable();
			$table->dateTime('create_date')->nullable();
			$table->dateTime('update_date')->nullable();
			$table->integer('id_scheda')->nullable()->index('id_scheda_contatti_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sch_contatti');
	}

}
