<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRefCampiAnaTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ref_campi_ana_test', function(Blueprint $table)
		{
			$table->foreign('id_ana_tipologie_valori_test', 'id_tipologie_valori_test')->references('id')->on('ana_tipologie_valori_test')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ref_campi_ana_test', function(Blueprint $table)
		{
			$table->dropForeign('id_tipologie_valori_test');
		});
	}

}
