<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DatatablesController extends Controller
{
  public function getIndex()
  {
    return view('admin.accessi.index');
  }

  public function Accessi()
  {
      return Datatables::of(Accessi::query())->make(true);
  }
}
