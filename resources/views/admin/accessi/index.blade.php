@extends('layoutmaster')

@section('title', 'Gestione Accessi')

@push('style')
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
@endpush

@section('section')

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      @include('admin.header.header');

      @include('admin.sidebar.sidebar');

    <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">


      @include('admin.header.breadcrumb');

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="accessi" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Cognome</th>
                      <th>Nome</th>
                      <th>E-mail</th>
                      <th>Livello</th>
                      <th>Creato</th>
                      <th>Ultimo Login</th>
                      <th>Attivo</th>
                    </tr>
                    </thead>

                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>

      </div>
      <!-- /.content -->

      @include('admin.footer.footer');

      @include('admin.sidebar.controlsidebar');

    </div>
@endsection

@push('script')
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="plugins/fastclick/fastclick.js"></script>
  <script type="text/javascript">
  $(function() {
      $('#accessi').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! URL::asset('accessi/data') !!}',
          columns: [
              { data: 'id', name: 'id' },
              { data: 'cognome', name: 'cognome' },
              { data: 'nome', name: 'Nome' },
              { data: 'email', name: 'email' },
              { data: 'livello', name: 'livello'},
              { data: 'insert_date', name: 'insert_date' },
              { data: 'last_login', name: 'last_login' },
              { data: 'attivo', name: 'attivo' }
          ]
      });
  });
  </script>

@endpush
