<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAnaAccessiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ana_accessi', function(Blueprint $table)
		{
			$table->foreign('scheda_id', 'id_scheda')->references('id')->on('ana_schede')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ana_accessi', function(Blueprint $table)
		{
			$table->dropForeign('id_scheda');
		});
	}

}
