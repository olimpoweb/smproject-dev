<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnaAccessiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ana_accessi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 100)->nullable();
			$table->string('password', 160)->nullable();
			$table->dateTime('insert_date')->nullable();
			$table->dateTime('update_date')->nullable();
			$table->boolean('attivo')->nullable()->default(0);
			$table->integer('scheda_id')->index('id_scheda');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ana_accessi');
	}

}
