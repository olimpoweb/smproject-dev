<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnaTipologieValoriTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ana_tipologie_valori_test', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('tipologia')->nullable();
			$table->string('input_type')->nullable();
			$table->boolean('attivo')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ana_tipologie_valori_test');
	}

}
