<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefCampiAnaTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ref_campi_ana_test', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_ana_tipologie_valori_test')->nullable()->index('id_tipologie_valori_test_idx');
			$table->string('nome_campo')->nullable();
			$table->boolean('posizione')->nullable()->default(0);
			$table->boolean('richiesto')->nullable()->default(0);
			$table->boolean('attivo')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ref_campi_ana_test');
	}

}
