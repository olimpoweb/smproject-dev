<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAnaSchedeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ana_schede', function(Blueprint $table)
		{
			$table->foreign('livello_id', 'id_livello_scheda')->references('id')->on('ana_livelli')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ana_schede', function(Blueprint $table)
		{
			$table->dropForeign('id_livello_scheda');
		});
	}

}
