<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard', function()
{
  return view('admin.dashboard');
});

Route::get('/login', function()
{
  return view('login');
});


// Route gestione Accessi
Route::get('accessi', 'AccessiController@index');
Route::get('accessi/data', 'AccessiController@data');
