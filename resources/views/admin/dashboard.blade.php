@extends('layoutmaster')

@section('title', 'Home Page')

@section('section')

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      @include('admin.header.header')

      @include('admin.sidebar.sidebar')

      <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

        </section>

      </div>

      @include('admin.footer.footer')

      @include('admin.sidebar.controlsidebar')

    </div>

@stop

@push('script')
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
@endpush
