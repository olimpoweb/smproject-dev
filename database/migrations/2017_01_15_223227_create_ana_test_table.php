<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnaTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ana_test', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tipologia_id')->nullable();
			$table->string('descrizione')->nullable();
			$table->boolean('attivo')->nullable()->default(0);
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ana_test');
	}

}
