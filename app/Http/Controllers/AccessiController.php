<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Accessi;
use Yajra\Datatables\Datatables;
use Carbon;

class AccessiController extends Controller
{

    //Index
    public function index()
    {
      // $AccessiList = Accessi::orderBy('email', 'asc')->get();

      return view('admin.accessi.index');
      // return view('admin.accessi.index')->json('AccessiList', $AccessiList);
    }

    public function data()
    {
      $data = new Accessi();
      $accessi = $data->loadlist();


      return Datatables::of($accessi)
      ->editColumn('insert_date', function ($accessi) {
        return $accessi->created_at ? with(new Carbon($accessi->created_at))->format('d-m-Y') : '';
      })
      ->editColumn('last_login', function ($accessi) {
        return $accessi->last_login ? with(new Carbon($accessi->last_login))->format('d-m-Y H:i:s') : '';
      })
      ->editColumn('attivo', function ($accessi) {
        return ($accessi->attivo == 1) ? '<span data-skin="skin-green" class="btn btn-success btn-xs"><i class="fa fa-check-circle" aria-hidden="true"></i></span>' : '<span data-skin="skin-red" class="btn btn-warning btn-xs"><i class="fa fa-check-circle bg-red" aria-hidden="true"></i></span>';
      })
      ->make(true);
    }

    public function Schede()
    {
      return $this->hasOne('App\Schede');
    }
}
