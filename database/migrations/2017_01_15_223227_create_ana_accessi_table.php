<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnaAccessiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ana_accessi', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 100)->nullable();
			$table->string('password', 160)->nullable();
			$table->dateTime('created_at')->nullable();
			$table->dateTime('last_login')->nullable();
			$table->boolean('attivo')->nullable()->default(0);
			$table->integer('scheda_id')->index('id_scheda');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ana_accessi');
	}

}
