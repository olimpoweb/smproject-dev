<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSchFisicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sch_fisico', function(Blueprint $table)
		{
			$table->foreign('id_scheda', 'id_scheda_fisico')->references('id')->on('ana_schede')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sch_fisico', function(Blueprint $table)
		{
			$table->dropForeign('id_scheda_fisico');
		});
	}

}
